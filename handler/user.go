package handler

import (
	"bwa/auth"
	"bwa/helper"
	"bwa/user"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type userHandler struct {
	userService user.Service
	authService auth.Service
}

func NewUserHandler(userService user.Service, authService auth.Service) *userHandler{
	return &userHandler{userService,authService}
}

func (h *userHandler) RegisterUser(c *gin.Context){
	var input user.RegisterUserInput
	
	err := c.ShouldBindJSON(&input) 
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors":errors} // map

		response := helper.APIResponse("Registered account failed", http.StatusUnprocessableEntity,"error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity,response)
		return
	}

	newUser, err := h.userService.ResgisterUser(input)
	if err != nil {
		response := helper.APIResponse("Registered account failed", http.StatusBadRequest,"error", nil)
		c.JSON(http.StatusBadRequest,response)
		return
	}

	token, err := h.authService.GenerateToken(newUser.ID)
	if err != nil {
		response := helper.APIResponse("Registered account failed", http.StatusBadRequest,"error", nil)
		c.JSON(http.StatusBadRequest,response)
		return
	}

	formatter := user.FormatUser(newUser,token)
	response := helper.APIResponse("Account has been registered", http.StatusOK,"success", formatter)
	c.JSON(http.StatusOK,response)
}

func (h* userHandler) Login(c *gin.Context){
	//user input (email,password)
	//input ditangkap handler
	//mapping dari input user ke input struct
	//di service mencari dgn bantuan repository user dengan email x
	//mencocokan password
	var input user.LoginInput

	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}

		response := helper.APIResponse("Login failed",http.StatusUnprocessableEntity,"error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity,response)
		return
	}

	loggedInUser, err := h.userService.Login(input)
	if err != nil {
		errorMessage := gin.H{"errors": err.Error()}

		response := helper.APIResponse("Login failed",http.StatusUnprocessableEntity,"error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity,response)
		return
	}

	token, err := h.authService.GenerateToken(loggedInUser.ID)
	if err != nil {
		response := helper.APIResponse("Login failed", http.StatusBadRequest,"error", err.Error())
		c.JSON(http.StatusBadRequest,response)
		return
	}

	formatter := user.FormatUser(loggedInUser,token)
	response := helper.APIResponse("Success login", http.StatusOK,"success", formatter)
	c.JSON(http.StatusOK,response)
}

func(h *userHandler) CheckEmailAvailablity(c *gin.Context){
	//input email dari user
	//input email di mapping ke struct input
	//struct input di parsing ke service
	//service memanggil repository - email sudah ada atau belum
	//repository - query db
	var input user.CheckEmailInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}

		response := helper.APIResponse("Email is not available",http.StatusUnprocessableEntity,"error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity,response)
		return
	}

	isEmailAvailable,err := h.userService.IsEmailAvailable(input)
	if err != nil {
		errorMessage := gin.H{"errors": "Server error"}
		response := helper.APIResponse("Email is not available",http.StatusUnprocessableEntity,"error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity,response)
		return
	}

	data := gin.H{"is_available":isEmailAvailable}
	var metaMessage string
	if isEmailAvailable {
		metaMessage = "Email is Available"
	}else{
		metaMessage = "Email has been used"
	}

	response := helper.APIResponse(metaMessage,http.StatusOK,"success", data)
		c.JSON(http.StatusOK,response)
}

func(h *userHandler) UploadAvatar(c *gin.Context){
	//input dari user
	//simpan gambar di folder "images/"
	//di service kita panggil repo
	//JWT (harcode sementara)
	//repo ambil data user yg ID = x
	//repo update data user simpan lokasi file

	file, err := c.FormFile("avatar")
	if err != nil {
		data := gin.H{"is_uploaded":false}
		response := helper.APIResponse("Failed upload avatar image",http.StatusBadRequest,"error",data)
		c.JSON(http.StatusBadRequest,response)
		return
	}

	currentUser := c.MustGet("currentUser").(user.User)
	userID := currentUser.ID //dapat dari JWT

	path := fmt.Sprintf("images/%d-%s",userID,file.Filename)

	err = c.SaveUploadedFile(file,path)
	if err != nil {
		data := gin.H{"is_uploaded":false}
		response := helper.APIResponse("Failed upload avatar image",http.StatusBadRequest,"error",data)
		c.JSON(http.StatusBadRequest,response)
		return
	}

	_,err = h.userService.SaveAvatar(userID,path)
	if err != nil {
		data := gin.H{"is_uploaded":false}
		response := helper.APIResponse("Failed upload avatar image",http.StatusBadRequest,"error",data)
		c.JSON(http.StatusBadRequest,response)
		return
	}

	data := gin.H{"is_uploaded":true}
	response := helper.APIResponse("Avatar successfully uploaded",http.StatusOK,"success",data)
	c.JSON(http.StatusOK,response)
}

func (h *userHandler) FetchUser(c *gin.Context)  {
	currentUser := c.MustGet("currentUser").(user.User)

	formatter := user.FormatUser(currentUser,"")

	response := helper.APIResponse("Successfully fetch user data",http.StatusOK,"success",formatter)
	c.JSON(http.StatusOK,response)
}