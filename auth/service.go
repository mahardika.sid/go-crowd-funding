package auth

import (
	"errors"
	"os"

	"github.com/dgrijalva/jwt-go"
)

type Service interface {
	GenerateToken(userID int) (string, error)
	ValidateToken(token string) (*jwt.Token, error)
}

type jwtService struct {
}

func NewService() *jwtService{
	return &jwtService{}
}

func (s *jwtService) GenerateToken(userID int) (string, error) {
	claim := jwt.MapClaims{}
	claim["user_id"] = userID

	if os.Getenv("SECRET_KEY")=="" {
		return "",errors.New("Invalid Secret")
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256,claim)
	signedToken,err := token.SignedString([]byte(os.Getenv("SECRET_KEY")))
	if err != nil {
		return signedToken, err
	}
	return signedToken,err
}

func (s *jwtService) ValidateToken(encodedToken string) (*jwt.Token,error){
	token,err := jwt.Parse(encodedToken,func(t *jwt.Token) (interface{}, error) {
		_,ok := t.Method.(*jwt.SigningMethodHMAC)

		if !ok{
			return nil,errors.New("Invalid Token")
		}

		if os.Getenv("SECRET_KEY")=="" {
			return nil,errors.New("Invalid Secret")
		}

		return []byte(os.Getenv("SECRET_KEY")),nil
	})
	
	if err != nil {
		return token, err
	}

	return token,nil
}