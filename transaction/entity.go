package transaction

import (
	"bwa/campaign"
	"bwa/user"
	"time"
)

type Transaction struct {
	ID         int
	CampaignID int
	UserId     int
	Amount     int
	Status     string
	Code       string
	User       user.User
	Campaign   campaign.Campaign
	PaymentURL string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}
